﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M1L3_TIO_SLN
{ // Try it out! Create a new console application and add a single class called AreaCalculator.
    // It should contain two methods for calculating the area of a circle and a triangle.
    // Then add a unit test project to provide testing.
    public class AreaCalculator
    {
        public double CircleArea(double r)
        {
            double area = 3.14159265359 * (r * r);

            return Math.Round(area,2);
        }

        public double TriangleArea(double b, double h)
        {
            double area = (b * h) * 0.5;

            return area;
        }

        public double HexagonArea(double s)
        {

            double area = ((3 * Math.Sqrt(3)) / 2) * (s * s);

            return Math.Round(area,2);

        }
    }
}
