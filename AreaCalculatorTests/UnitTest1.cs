using M1L3_TIO_SLN;
using System;
using Xunit;

namespace AreaCalculatorTests
{
    // Try it out! Create a new console application and add a single class called AreaCalculator.
    // It should contain two methods for calculating the area of a circle and a triangle.
    // Then add a unit test project to provide testing.
    public class UnitTest1
    {
        [Fact]
        public void CircleArea_ShouldReturnAreaOfCircle()
        {
            // Arrange
            double r = 1.5;
            double expected = 7.07;
            AreaCalculator areaCalculator = new AreaCalculator();

            // Act
            double actual = areaCalculator.CircleArea(r);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TriangleArea_ShouldReturnAreaOfTriangle()
        {
            // Arrange
            double left = 1.5;
            double right = 2;
            double expected = 1.5;
            AreaCalculator areaCalculator = new AreaCalculator();

            // Act
            double actual = areaCalculator.TriangleArea(left,right);

            // Assert
            Assert.Equal(expected, actual);
        }

        // Try it out!* Write a unit test for a method which calculates the area of a hexagon.Then write the method to pass the test.
        [Fact]
        public void HexagonArea_ShouldReturnAreaOfHexagon()
        {
            // Arrange
            double s = 1.5;
            double expected = 5.85;
            AreaCalculator areaCalculator = new AreaCalculator();

            // Act
            double actual = areaCalculator.HexagonArea(s);

            // Assert
            Assert.Equal(expected, actual);
        }

    }
}
